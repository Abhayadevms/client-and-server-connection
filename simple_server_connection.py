import socket

server_socket =socket.socket()       #connect socket #open socket
server_socket.bind(("",2002))        #bind the port & localhost
server_socket.listen()               #listen server
conn,addr = server_socket.accept()   # accept server
data = conn.recv(1000)               #data .#define byte reciving
print(data.decode())                       #decode recive data
conn.sendall("hey from server".encode())   #encode the data send  data
conn.close()                    #connection close
server_socket.close()           #socket close