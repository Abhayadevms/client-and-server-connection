import socket

client_socket = socket.socket()                         #connect socket
port_number = 2002                                      #define port
client_socket.connect(("localhost",2002))               #connect the port & localhost
client_socket.sendall("hi server itz your client".encode())  #encode the sending message and send
data = client_socket.recv(1000)                          #define reciving byte and data
print(data.decode())                                     #decode the reciving data and print
client_socket.close()                                    #close the client socket
